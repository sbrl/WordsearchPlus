"use strict";

// This doesn't work - I suspect webpack is required
// For now we're including it via a script tag in index.html
//import Futility from './node_modules/futility/dist/futility.js'

import { random } from './Random.mjs';
import { iterate_indexes, iterate_directions_random } from './IterableHelpers.mjs';

// From https://stackoverflow.com/a/1431113/1460422
String.prototype.replaceAt = function(index, replacement) {
	return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

class WordsearchGenerator
{
	constructor(mask_image = null)
	{
		// TODO: Make this more inteelligent. We should be able to calculate whether a given wordlist will fit inside the given mask somehow mathematically
		this.max_tries_per_word = 100; // Try to place a word 100 times before giving up
		this.backwards_words_allowed = false; // Whether to allow words to be placed backwards or not
		// Whether to check for and remove rude words. Very slow.
		this.rude_words_check = true;
		// The pool of charactes to fill in the gaps from
		this.character_fill_pool = "qwertyuiopasdfghjklzxcvbnm";
		
		if(mask_image != null) // Allow creation of a blank generator
			this.update_mask_image(mask_image);
	}
	
	update_mask_image(mask_image) {
		this.width = mask_image.width;
		this.height = mask_image.height;
		this.alpha_map = this.pixmap_to_alpha_map(
			this.image_to_pixmap(mask_image)
		);
	}
	
	pixmap_to_alpha_map(pixmap) {
		var alpha_map = [];
		
		for(let i = 0; i < pixmap.length; i += 4)
			alpha_map.push(pixmap[i + 3]);
		
		return alpha_map;
	}
	
	image_to_pixmap(image) {
		let canvas = document.createElement("canvas"),
			context = canvas.getContext("2d");
		
		canvas.width = image.width;
		canvas.height = image.height;
		context.drawImage(image, 0, 0);
		return context.getImageData(0, 0, canvas.width, canvas.height).data;
	}
	
	/**
	 * Renders a new wordsearch with the specified wordlist.
	 * @param  {string[]} wordlist An array of words to include.
	 * @return {{wordsearch:[[string]], failed_placements:number}}        An object containing the rendered wordlist as a 2-dimensional array of characters, and the number of words that couldn't be placed in the wordsearch.
	 */
	render(wordlist) {
		let wordsearch = this.generate_wordsearch_space(this.width, this.height);
		
		let failed_placements = 0;
		let words_placed = [];
		for (let word of wordlist) {
			if(!this.place_word(wordsearch, word)) {
				failed_placements++;
			}
			else {
				words_placed.push(word);
			}
		}
		
		console.log("Wordsearch: ");
		this.log_wordsearch(wordsearch);
		
		// Generate a placement mask to let the rude words solverknow which characters it can't change
		let placement_mask = this.generate_placement_mask(wordsearch);

		console.log("Placement mask: ");
		this.log_wordsearch(placement_mask);

		// Fill in the gaps in the wordsearch.
		// This has to be done _after_ the creation of the mask, as it fills in
		// cells outside the mask with a single space to enable the rude words
		// checker to do its job.
		this.fill_in_gaps(wordsearch, placement_mask);
		
		
		if(this.rude_words_check) {
			this.remove_rude_words(wordsearch, placement_mask);
		}
		
		return {
			wordsearch,
			placement_mask,
			words_placed,
			failed_placements
		};
	}
	
	generate_wordsearch_space(width, height) {
		let result = [];
		
		for(let y = 0; y < height; y++)
			result.push(" ".repeat(width).split("").map(char => ""));
		
		return result;
	}
	
	place_word(wordsearch, word) {
		
		let s_pos = null;
		let dir = null;
		
		// Generate a valid placement
		
		let tries = 0;
		do {
			// Don't keep on trying forever
			if(tries > this.max_tries_per_word)
				return false;
			
			s_pos = { x: random(0, this.width, false), y: random(0, this.height, false) };
			for(dir of iterate_directions_random(this.backwards_words_allowed)) {
				if(this.is_placement_valid(wordsearch, word, s_pos, dir))
					break; // This works because the outer loop will pick up on it automagically
			}
			
			tries++;
		} while(!this.is_placement_valid(wordsearch, word, s_pos, dir));
		
		console.log(`Placing '${word}' starting at (${s_pos.x}, ${s_pos.y}) in direction (${dir.x}, ${dir.y}) after ${tries} attempts`);
		
		// Place the word, recording the cells in which we placed characters 
		// for further checks later on
		for(let i = 0; i < word.length; i++) {
			let next_pos = {
				x: s_pos.x + i * dir.x,
				y: s_pos.y + i * dir.y
			};
			wordsearch[next_pos.y][next_pos.x] = word[i];
		}
		
		return true;
	}
	
	/**
	 * Fills in the gaps in a wordsearch according to the mask, drawing from 
	 * this.character_fill_pool.
	 * Note that this also replaces the spots outside the wordsearch with a 
	 * single space, to enable the rude words checker to do its job.
	 * @param	{[[string]]}	wordsearch		The wordsearch to fill.
	 * @param	{[[string]]}	placement_mask	The placement mask to use to avoid overwriting hiidden words.
	 */
	fill_in_gaps(wordsearch, placement_mask) {
		for(let y = 0; y < wordsearch.length; y++) {
			for(let x = 0; x < wordsearch[y].length; x++) {
				// Avoid overwriting hidden words
				if(placement_mask[y][x] == "x") continue;
				
				wordsearch[y][x] = this.is_pos_masked(x, y) ? " " : this.character_fill_pool[random(0, this.character_fill_pool.length, false)];
			}
		}
	}
	
	/**
	 * Generates a 2-dimensional mask representing where words have been placed in a wordsearch.
	 * @param	{[[string]]}	wordsearch	The wordsearch to generate the mask for.
	 * @return	{[[string]]}	A mask of where words have been placed in the specified wordsearch
	 */
	generate_placement_mask(wordsearch) {
		// FUTURE: We could count the number of times we place a word at a position by constructing this when placing words - then we could highlight on the answer sheet more betterer :D
		let mask = [];
		for(let y = 0; y < wordsearch.length; y++) {
			mask.push(wordsearch[y].map((char) => char.length > 0 ? "x" : " "));
		}
		return mask;
	}
	
	remove_rude_words(wordsearch, placement_mask) {
		let checker = new Futility();
		
		// We have to do it one at a time here, because we might end up creating
		// another bad word inadvertantly by scrambling an existing one
		let fixed_bad_word = false;
		do {
			fixed_bad_word = this.remove_rude_word_once(wordsearch, placement_mask, checker);
		} while(fixed_bad_word);
	}
	
	/**
	 * Selectively alters the letters along a straight path.
	 * @param	{[[string]]}	wordsearch		The wordsearch to operate on.
	 * @param	{[[string]]}	placement_mask	The placement mask reference to avoid changing characters that are part of hidden words.
	 * @param	{{x, y}}		s_pos			The starting position.
	 * @param	{{x, y}}		dir				The direction in which to travel.
	 * @param	{string}		word_mask		The word mask to use to determine which positions to alter. Only '*' characters will be altered.
	 * @return	{number}		The number of characters replaced.
	 */
	replace_under_mask(wordsearch, placement_mask, s_pos, dir, word_mask) {
		let replaced_letters = 0;
		
		for(let i of iterate_indexes(word_mask, "*")) {
			// If there aren't any matches at all, then there's nothing to do
			if(i == -1) break;
			
			let next_pos = {
				x: s_pos.x + i * dir.x,
				y: s_pos.y + i * dir.y
			};
			
			// Don't touch characters that are part of hidden words
			if(placement_mask[next_pos.y][next_pos.x] == "x")
				continue;
			
			// Scramble the character
			wordsearch[next_pos.y][next_pos.x] = this.character_fill_pool[random(0, this.character_fill_pool.length, false)];
			
			replaced_letters++;
		}
		
		return replaced_letters;
	}
	
	extract_strip(wordsearch, s_pos, dir) {
		let result = "";
		
		let i = 0, c_pos = { x: s_pos.x, y: s_pos.y };
		// While we're still in-bounds....
		while(c_pos.x >= 0 && c_pos.y >= 0 && c_pos.x < wordsearch[0].length && c_pos.y < wordsearch.length) {
			// Keep adding the letter we find to the result
			result += wordsearch[c_pos.y][c_pos.x];
			
			c_pos = {
				x: s_pos.x + i * dir.x,
				y: s_pos.y + i * dir.y
			};
			i++;
		}
		
		return result;
	}
	
	remove_rude_word_once(wordsearch, placement_mask, checker) {
		// This method works only because the gaps filler replaces cells 
		// outside the wordsearch mask area with a single space
		
		// Horizontal
		for(let y = 0; y < wordsearch.length; y++) {
			let row_text = wordsearch[y].join("");
			
			if(checker.test(row_text)) {
				let chars_changed = this.replace_under_mask(
					wordsearch,
					placement_mask,
					{ x: 0, y },
					{ x: 1, y: 0 },
					checker.censor(row_text, "*")
				);
				
				// It's remotely possible that we've found a 'bad' word that's 
				// actually part of a hidden word. This ensures that we don't 
				// experience any unfortunate loops
				if(chars_changed > 0)
					return true;
			}
			
			
			// Do it in reverse too
			// .slice(): shallow clone
			row_text = wordsearch[y].slice().reverse().join("")
			if(checker.test(row_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					{ x: 0, y: wordsearch[y].length - 1 }, // Start at the last cell in the row
					{ x: -1, y: 0 }, // Go backwards
					checker.censor(row_text, "*")
				) > 0) return true;
			}
		}
		
		// Vertical
		for(let x = 0; x < wordsearch[0].length; x++) {
			let col_text = wordsearch.map((row) => row[x]).join("");
			if(checker.test(col_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					{ x, y: 0 }, // Start at the first cell in the column
					{ x: 0, y: 1 }, // Go downwards
					checker.censor(col_text, "*")
				) > 0) return true;
			}
			
			// Don't forget to do it in reverse too
			// OPTIMIZE: Is there a faster way to reverse a string?
			col_text = col_text.split("").reverse().join("");
			if(checker.test(col_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					{ x, y: col_text.length - 1 }, // Start at the last cell in the column
					{ x: 0, y: -1 }, // Go upwards
					checker.censor(col_text, "*")
				) > 0) return true;
			}
		}
		
		// Diagonals top-left => bottom-right
		let s_pos = { x: 0, y: wordsearch.length - 1};
		while(s_pos.x < wordsearch[s_pos.y].length) {
			let strip_text = this.extract_strip(wordsearch, s_pos, { x: 1, y: 1 });
			
			if(checker.test(strip_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					s_pos, { x: 1, y: 1 },
					checker.censor(strip_text, "*")
				) > 0) return true;
			}
			
			// Don't forget to do it in reverse too
			// OPTIMIZE: Is there a faster way to reverse a string?
			strip_text = strip_text.split("").reverse().join("");
			if(checker.test(strip_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					{ x: s_pos.x + strip_text.length - 1, y: s_pos.y + strip_text.length - 1 },
					{ x: -1, y: -1 },
					checker.censor(strip_text, "*")
				) > 0) return true;
			}
			
			if(s_pos.y > 0)
				s_pos.y--;
			else
				s_pos.x++;
		}
		
		// Diagonals top-right => bottom-left
		s_pos = { x: wordsearch[0].length - 1, y: wordsearch.length - 1};
		while(s_pos.x < wordsearch[s_pos.y].length) {
			let strip_text = this.extract_strip(wordsearch, s_pos, { x: -1, y: 1 });
			
			if(checker.test(strip_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					s_pos, { x: -1, y: 1 },
					checker.censor(strip, "*")
				) > 0) return true;
			}
			
			// Don't forget to do it in reverse too
			// OPTIMIZE: Is there a faster way to reverse a string?
			strip_text = strip_text.split("").reverse().join("");
			if(checker.test(strip_text)) {
				if(this.replace_under_mask(
					wordsearch,
					placement_mask,
					{ x: s_pos.x - strip_text.length - 1, y: s_pos.y - strip_text.length - 1 },
					{ x: 1, y: -1 },
					checker.censor(strip_text, "*")
				) > 0) return true;
			}
			
			if(s_pos.y > 0)
				s_pos.y--;
			else
				s_pos.x++;
		}
		
		return false;
	}
	
	
	is_pos_masked(x, y) {
		return this.alpha_map[y*this.width + x] < 128;
	}
	
	is_placement_valid(wordsearch, word, s_pos, dir) {
		for(let i = 0; i < word.length; i++) {
			let next_pos = {
				x: s_pos.x + i * dir.x,
				y: s_pos.y + i * dir.y
			};
			
			// If we're out-of-bounds, then we can't be bothered
			if(next_pos.x < 0 || next_pos.y < 0 || next_pos.x >= wordsearch[0].length || next_pos.y >= wordsearch.length)
				return false;
			
			// If it's masked, we're not interested
			if(this.is_pos_masked(next_pos.x, next_pos.y))
				return false;
			
			// If the cell isn't empty (or equal to the current character in the word), then we can't place the word here
			if(wordsearch[next_pos.y][next_pos.x] !== "" && wordsearch[next_pos.y][next_pos.x] !== word[i])
				return false;
		}
		
		// We've checked every spot and nothing seems to have gone wrong, so it 
		// must be valid
		return true;
	}
	
	log_wordsearch(wordsearch) {
		console.log(wordsearch.map((row) => row.map((char) => char.length > 0 ? char : " ").join(" ")).join("\n"));
	}
}

export default WordsearchGenerator;
