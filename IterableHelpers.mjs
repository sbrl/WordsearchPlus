"use strict";

/**
 * Shuffles array in place with the Fisher-Yates algorithm.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

/**
 * Iterates over the locations of a specified target.
 * @param	{array|string}	thing	The thing to search. May be either a string or an array.
 * @param	{mixed}			target	The target to search for.
 * @return	{Generator}		A generator that iterates over the index(es) of the provided target.
 */
function* iterate_indexes(thing, target) {
	let mark = 0, next_pos = -1;
	do {
		next_pos = thing.indexOf(target, mark);
		yield next_pos;
		mark = next_pos + 1;
	} while(next_pos != -1);
}

function* iterate_directions_random(backwards_words_allowed = true) {
	let directions = [
		{ x: 0, y: -1 },
		{ x: 1, y: -1 },
		{ x: 1, y: 0 },
		{ x: 1, y: 1 },
		{ x: 0, y: 1 },
		{ x: -1, y: 1 },
		{ x: -1, y: 0 },
		{ x: -1, y: -1 },
	];
    if(!backwards_words_allowed) {
        directions = directions.filter((el) => el.x >= 0 && el.y >= 0);
    }
	shuffle(directions);
	
	for(let dir of directions)
		yield dir;
}

export { iterate_indexes, iterate_directions_random };
